import paho.mqtt.client as mqtt
import time
from communications import sendMsgGUI as smg
import pandas as pd
import datetime
from tkinter import *

#on_connect, on_message, on_subscribe are inbuilt callbacks of the Paho that are programmable
def on_connect(client,userdata,flags,rc):
    print("on_connect")
    if rc == 0: #rc=0 marks a successful connection of the client to the broker.
        client.connected_flag=True #set flag
        print("Connected Ok")
    else:
        print("Bad Connection, returned code=",rc)

        
def on_message(client,userdata,message):# On recieving the message
    if str(message.topic)!=df["Contact"][0]:
        msg=str(message.payload.decode("utf-8"))
        sendersMessage(msg)
        
    else: #Remove this in the final application
        msg=str(message.payload.decode("utf-8"))
        sendersMessage(msg)

def on_subscribe(client,userdata,mid,granted_qos):
    print("Subscribed",str(mid),str(granted_qos))

#Function to write the recieved message to the comm file
def sendersMessage(msg):
    file=open("comm.txt","a")
    #file.write("\n"+'[Sender:'Contact+"]\t"+Time and date of recieval of message+"\n"+Message)
    file.write("\n"+'[Sender:'+msg[:10]+"]\t"+str(datetime.datetime.now())+"\n"+msg[10:])
    file.close()

# Function to send the mussage to the reciever and write the details of the reciever, date,
#time and the messgae to the file
def sendMsg(msg,recep,name):
    recep=str(recep)
    global df
    message=str(df["Contact"][0])
    message+=msg.get()
    '''
    subtop=df["Contact"][0]
    subtop=str(subtop)   
    client.subscribe(subtop)#Remember to automate this
    '''
    file=open("comm.txt","a")
    grpContList=[]
    if(recep!="group"):
        client.publish(recep,message)
        file.write("\nReceiver:"+recep+"\t"+str(datetime.datetime.now())+"\n"+"[You]:"+message[10:])
        file.close()
    else: 
        df=pd.read_excel("Groups.xls","group")
        flag=0
        for i in range(0,len(df)):
            if(name==df["Name"][i]):
                flag=1
                grpContList.append(str(df["contacts"][i]))
            if(flag==1 and name!=df["Name"][i]):
                flag=0
                break
        for con in grpContList:
            client.publish(con,message)

        file.write("Receiver:"+name+"\t"+str(datetime.datetime.now())+"\n"+"[You]:"+message)
        file.close()

#Line to crete the connection and subscribe to the our own contact so that we can recieve messgaes
global df
df=pd.read_excel("Contact.xls","Contact")
client=mqtt.Client()
broker_addr="iot.eclipse.org"
port=1883
client.on_connect=on_connect
client.on_message=on_message
time.sleep(1)
client.connect(broker_addr,port)
client.loop_start()
subtop=df["Contact"][0]
subtop=str(subtop)
client.subscribe(subtop)#Remember to automate this




