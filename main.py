from tkinter import *
import pandas as pd
import time
#Importing the self defined modules
from createGroup import createGroupGUI as gc
from addingContact import contactListGUI as cl
from communications import sendMsgGUI as sm

#Close the application
def end():
    exit(0)

#To call the actual messgae functionality with the Name and the Number of the recepient
def Message(serial):
    try:
        print(serial.get())
        recep=int(serial.get())
        print(df["Names"][recep-1])
        print(df["Contact"][recep-1])
        print(type(df["Names"][recep-1]))
        sm.message(df["Names"][recep-1],df["Contact"][recep-1])
    except ValueError:
        print("Error A Gaya")        
    
#Function to refresh the list of contacts

def refresh(textArea):
    textArea.delete("1.0",END)
    df=pd.read_excel("Contact.xls","Contact")
    count=0
    l=df["Names"]
    c=df["Contact"]
    for i in range(0,len(df)):
        textArea.insert(END,str(count)+"\t"+l[i]+"\t"+str(c[i])+"\n")
        count+=1


df=pd.read_excel("Contact.xls","Contact")
l=df["Names"]
c=df["Contact"]

root=Tk()
lab= Label(root,text="Contacts")
lab.grid(row=0,column=0)
textArea=Text(root)
textArea.grid(row=1,column=0)
count=1
for i in range(0,len(df)):
        textArea.insert(END,str(count)+"\t"+l[i]+"\t"+str(c[i])+"\n")
        count+=1

lab= Label(root,text="Enter the serial number of the contact to send message")
lab.grid(row=2,column=0)

serial=Entry(root)
serial.grid(row=3,column=0)

bottomFrame = Frame(root)
bottomFrame.grid(row=4,column=0)
 
createGrp=Button(bottomFrame,text="Create Group",command=gc.createGrp)
createGrp.pack(side=LEFT)

addContact=Button(bottomFrame,text="Add Contact",command=cl.main)
addContact.pack(side=LEFT)

sendMsg=Button(bottomFrame,text="Send Message",command=lambda:Message(serial))
sendMsg.pack(side=LEFT)

ref=Button(bottomFrame,text="Refresh",command=lambda:refresh(textArea))
ref.pack(side=LEFT)

exi=Button(bottomFrame,text="exit",command=end)
exi.pack(side=LEFT)

root.mainloop()
