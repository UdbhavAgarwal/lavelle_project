import pandas as pd
from tkinter import *
from pathlib import Path
import xlwt

#Function to clear the text fields
def clear(entName,entCon,entMail):
    entName.delete(0,END)
    entCon.delete(0,END)
    entMail.delete(0,END)

#Error Function to throw errors
def error(eMsg):
    root1=Tk()
    labName=Label(root1,text=eMsg)
    labName.grid(row=0,column=1)
    okBtn=Button(root1,text="OK",command=lambda:root1.destroy())
    okBtn.grid(row=1,column=1)

#The Main registration function 
def submit(entName,entCon,entMail):
    name=entName.get()
    contact=entCon.get()
    mail=entMail.get()
    if(len(contact)==10):
        try:
            parseCon=int(contact)# Done to check that the number entered should have only numbers
            my_file1=Path("comm.txt")
            my_file2=Path("Contact.xls")
            my_file3=Path("Groups.xls")
            if(((my_file1.is_file())==False) and ((my_file2.is_file())==False) and ((my_file3.is_file())==False)):
                #create the comm file
                file=open("comm.txt","w")
                file.close() #This will create the file

                #create the contact.xls file
                wb=xlwt.Workbook()
                ws=wb.add_sheet("Contact")
                ws.write(0,0,"Contact")
                ws.write(0,1,"Names")
                ws.write(1,0,contact)
                ws.write(1,1,name)
                wb.save("Contact.xls")
                
                #create the group.xls file
                wb=xlwt.Workbook()
                ws=wb.add_sheet("group")
                ws.write(0,0,"Contacts")
                ws.write(0,1,"Name")
                wb.save("Groups.xls")
                
                
            else:
                error("You are already registered")
                
        except:
            error("Number is invalid")

#The GUI of the file
root=Tk()

labName=Label(root,text="Name")
labName.grid(row=0,column=0)
entName=Entry(root)
entName.grid(row=0,column=1)

labCon=Label(root,text="Contact")
labCon.grid(row=1,column=0)
entCon=Entry(root)
entCon.grid(row=1,column=1)

labMail=Label(root,text="Email")
labMail.grid(row=2,column=0)
entMail=Entry(root)
entMail.grid(row=2,column=1)

submitBtn=Button(root,text="Submit",command=lambda:submit(entName,entCon,entMail))
submitBtn.grid(row=3,column=1)

clearBtn=Button(root,text="Clear",command=lambda:clear(entName,entCon,entMail))
clearBtn.grid(row=3,column=0)

exitBtn=Button(root,text="Exit",command=lambda:root.destroy())
exitBtn.grid(row=3,column=2)

root.mainloop()
