                                                                       Mitr Toli (The Friends' Adda)

The company gave the task for creating a Chatting Application very much like the well known WhatsApp, Slack etc.
My idea of the application is inspired by observing such applications. My application will provide it’s users features like Registering themselves, 
Adding New Contacts, Creating Groups, Sending Group Messages and Sending Personal Messages. 
I have used the MQTT Protocol for the communications. MQTT stands for Message Queuing Telemetry Transport.

MQTT Protocol:
The protocol works on the principle of Publish-Subscribe and is built over the TCP/IP protocol.The Sender publishes a topic and the receiver subscribes 
to it. When the sender needs to send a message, he publishes the message with the topic and all the users who have subscribed to the topic receive the message. 
The server in this process is known as the broker. In my application I am using the Public Server of the Eclipse Organization (iot.eclipse.org). 
Future development of the project will involve the creation of our own broker and hence providence of many more features to the users.

Use Cases:
The users are the people who register themselves on the applications. The use cases are Registering themselves, Adding New Contacts, Creating Groups, 
Sending Group Messages and Sending Personal Messages. 
The Sending Personal Messgaes and Group Messages will involve two or more users while other features involve only one user.

Design:

The project has been subdivided according to the functionalities. All the functionalities have been divided into separate folders.
These folders will contain a GUI file and one functionality file, it is so because The project has been further subdivided into GUI Part and the Functionality Part. 
The whole project can be controlled by the user using the main.py file i.e he can access all the functionalities of the system through this file. 
This file has only GUI in it and the GUIs of all the features are linked to the buttons on this file’s GUI. On clicking those buttons the functions 
in the respective files containing the requested feature’s GUI are invoked.

It will be my objective to keep the design as much Modularized and Layered as possible so that Extensibility and Scalability of the application could be increased. 
The Extensibility of the application increases as adding new modules and functionalities will be easy as we just need to create them and add there folder 
containing the GUI and the Functionality file and creating a simple GUI Button on the main.py file. 
Every feature is a folder (which can be viewed as a module) and the folder contains all the files related to the feature.

The Scalability of a project can be measured in many ways like the Vertical and the horizontal scalability, the scalability of the database. 
Horizontal scaling of the project is possible as new users can just register and use the application easily and in future as we develop our own broker 
we may be able to further distribute the server functionalities also and make the database centralized and remove the burden from the client side.
The Vertical Scaling of the project is also possible as new features can be easily added to the system as discussed above. 

The files required to contain the contact (contact.xls), group(Group.xls) and communications history (comm.txt) will be created at the time of registration.

Test Cases:
In my application I have used phone number of the users as the Topic to which they subscribe so there are restrictions possed on them therefore the test cases 
will test them. 
 
For Black Box Testing I am going to use two approaches:
1) Equivalance Class Partitioning
2) Boundary Value Analysis

Test CAses for Black Box teting
1) Equivalance Class Partitioniong Test Cases:

Test Cases for Equivalance Class Partitioning to check the length restriction of the contact number i.e the number should of 10 digits in length:

Test Suit 1:
Test Case 1: [1231,1234567809,12345678090]
Test Case 2: [123123,9532557799,01231230123]

Test Case 1: 
Input Parameters Name and contact , Name can be any, and the contact was given 1231, status: Failed as the length was not equal to 10.
Input Parameters Name and contact , Name can be any, and the contact was given 1234567809, status: Succesful as the length was equal to 10.
Input Parameters Name and contact , Name can be any, and the contact was given 12345678090, status: Failed as the length was not equal to 10.

Test Case 2: 
Input Parameters Name and contact , Name can be any, and the contact was given 123123, status: Failed (as the length was not equal to 10.)
Input Parameters Name and contact , Name can be any, and the contact was given 9532557799, Status: Succesful (as the length was equal to 10.)
Input Parameters Name and contact , Name can be any, and the contact was given 01231230123, Status: Failed (as the length was not equal to 10.)

2) Boundary Value Analysis to check the length restriction of the contact number i.e the number should of 10 digits in length:

Test Suit 2:
Test Case 1: [953255779] 
Input : Here we are giving 9 digits in the number [953255779], Status: Failed (as the number was not equal to 10)

Test Case 2: [69532557799] 
Input : Here we are giving 11 digits in the number [69532557799], Status: Failed (as the number was not equal to 10)

Test Suit 3:
[9532557959] 
Input : To check that the phone number that we are registering doesnot already exist I input a number that is already present, Status: Failed

Test Suit 4:
[Techies]
Input : To check that the group that we are creating doesnot already exist I input a group name that already exists, status: Failed


  

  
