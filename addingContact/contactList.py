import pandas as pd
#inmporting the GUI for this Feature
from addingContact import contactListGUI as clg

#Function to create the a new contact
def addContact(nameText,contactText):
    df=pd.read_excel("Contact.xls","Contact")
    name=nameText.get()
    con=contactText.get()
    if(len(con)==10):
        try:
            conParse=int(con)
            #This is done to check that wheather the user has entered a number only and not some random characters
            contact=df.to_dict()
            count=0
            numbers=list(contact["Contact"].values()) #Get the contacts in the dictionary in list format
            flag=0
            for num in numbers: # Check if number already exists
                count+=1
                if(con==str(num)):
                    clg.error("Contatct Already Exists")
                    flag=1
                    break
            if(flag==0): #It will execute if the number is not already present in the list
                contactDic={}
                nameLen=len(contact["Names"])
                conLen=len(contact["Contact"])
                #Add the name and number of the new entry at the end of the disctionary
                contact["Names"][len(contact["Names"])]=name
                contact["Contact"][len(contact["Contact"])]=con

                nameL=list(contact["Names"].values())
                conL=list(contact["Contact"].values())
                # Create a new dictionary having the new name and contact     
                contactDic["Names"]=nameL
                contactDic["Contact"]=conL
                #print(contactDic)
                #Convert to pandas dataframe and then to the excel sheet
                df=pd.DataFrame.from_dict(contactDic)
                df.to_excel("Contact.xls","Contact")
        except:
            print(type(conParse))
            print(conParse)
            clg.error("Please enter a valid Contact Number")        
    else:
        clg.error("The length of the contact should be equal to 10")



