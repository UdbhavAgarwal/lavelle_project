from tkinter import *
from addingContact import contactList as cl

#Close the current window
def close(root):
    root.destroy()
    
#Clear the text fields
def clear(nameText,contactText):
    nameText.delete(0,END)
    contactText.delete(0,END)

# To show whatever message we want to throw to the user
def error(msg):
    root1=Tk()
    nameLabel = Label(root1,text=msg)
    exitBtn = Button(root1,text="Ok",command=lambda:root1.destroy())
    nameLabel.grid(row=0,column=0)
    exitBtn.grid(row=1,column=0)

#The main GUI of the application
def main():       
    root=Tk()
    nameLabel = Label(root,text="Name")
    nameText=Entry(root)
    contactLabel = Label(root,text="Contact")
    contactText=Entry(root)

    addContactBtn = Button(root,text="Add Contact",command=lambda:cl.addContact(nameText,contactText))
    
    exitBtn = Button(root,text="Exit",command=lambda:close(root))

    clearBtn = Button(root,text="Clear",command=lambda:clear(nameText,contactText))

    addContactBtn.grid(row=2,column=0)

    clearBtn.grid(row=2,column=1)

    exitBtn.grid(row=2,column=2)

    nameLabel.grid(row=0,column=0)

    nameText.grid(row=0,column=1)

    contactLabel.grid(row=1,column=0)

    contactText.grid(row=1,column=1)
