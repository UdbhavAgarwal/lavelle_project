from tkinter import *
import pandas as pd
from createGroup import createGroup as cg



# The main GUI of the the Create a Group Feature
def createGrp():
    groupWin=Tk()
    groupWin.title("Create A Group")
    df=pd.read_excel("Contact.xls","Contact")
    lnames=df["Names"]
    lcontact=df["Contact"]
    lab= Label(groupWin,text="Contacts")
    lab.grid(row=0,column=0)
    textArea=Text(groupWin)
    textArea.grid(row=1,column=0)
    count=1
    for i in lnames:
        textArea.insert(END,str(count)+"\t"+i+"\n")
        count+=1
    lab= Label(groupWin,text="Enter the serial number of the contact you want to add to the group")
    lab.grid(row=2,column=0)

    serial=Entry(groupWin)
    serial.grid(row=3,column=0)
    
    addMem=Button(groupWin,text="Add member",command=lambda:cg.addMember(serial))
    addMem.grid(row=4,column=0)
    
    done=Button(groupWin,text="Done",command=Done)
    done.grid(row=4,column=1)
    
    groupWin.mainloop()

#Gui to Take the name of the group
def Done():
    root=Tk()
    root.title("Group Name")
    lab= Label(root,text="Group Name")
    lab.grid(row=0,column=0)
    grpName=Entry(root)
    grpName.grid(row=0,column=1)
    grp=Button(root,text="Create Group",command=lambda:cg.GrpFunc(grpName))
    grp.grid(row=4,column=0)

#Function to throw an error to the user
def error(eMsg):
    error=Tk()
    error.title("Error Message")
    lab= Label(error,text=eMsg)
    lab.grid(row=0,column=0)
    grp=Button(error,text="Ok",command=Done)
    grp.grid(row=1,column=1)
